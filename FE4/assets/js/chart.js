
var ctx = $("#bar-chartcanvas-1");;
var ctx1 = $("#doughnut-chartcanvas-1");

$(function(){
  var ctx1 = $("#doughnut-chartcanvas-1");
  var data1 = {
    labels: ["Computer Science", "History", "Mathematics", "Fictional", "Chemistry"],
    datasets: [
      {
        label: "TeamA Score",
        data: [10, 50, 25, 70, 40],
        backgroundColor: [
          "#aecef5",
          "#053166",
          "#e0a800",
          "#009bd9",
          "#2E8B57"
        ],
        borderColor: [
          "#CDA776",
          "#989898",
          "#CB252B",
          "#E39371",
          "#1D7A46"
        ],
        borderWidth: [1, 1, 1, 1, 1]
      }
    ]
  };



  var options = {
    responsive: true,
    title: {
      display: true,
      position: "top",

      fontSize: 18,
      fontColor: "#111"
    },
    legend: {
      display: true,
	  
      position: "right",
      labels: {
        fontColor: "#333",
        fontSize: 16
      }
    }
  };
    var ctx = $("#bar-chartcanvas-1");
 var data= {
        labels: ["Computer Science", "History", "Mathematics", "Fictional", "Chemistry"],
		 datasets: [
      {
       
        data: [20, 30, 25, 50, 55],
        backgroundColor: [
         "#aecef5",
          "#053166",
          "#e0a800",
          "#009bd9",
          "#2E8B57"
        ],
        borderColor: [
          "#CDA776",
          "#989898",
          "#CB252B",
          "#E39371",
          "#1D7A46"
        ],
        borderWidth: 1
      }
    ]
		
  }

  var chart1 = new Chart(ctx1, {
    type: "doughnut",
    data: data1,
    options: options
  });

  
var chart0 = new Chart(ctx, {
    type: 'bar',
    data: data,
    options: options
});

});
