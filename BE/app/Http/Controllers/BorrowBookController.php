<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\ReturnedBook;
use App\Models\BorrowedBook;
use Illuminate\Http\Request;
use App\Http\Requests\BorrowRequest;

class BorrowBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borrowedBook = BorrowedBook::with(['patron', 'book', 'book.category'])->get();
        return response()->json([
            "message" => "borrowedBook List",
             "data" => $borrowedBook]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BorrowRequest $request)
    {
        $borrowedBook = new BorrowedBook();

        $borrowedBook->copies = $request->copies;
        $borrowedBook->book_id = $request->book_id;
        $borrowedBook->patron_id = $request->patron_id;
        $validated = $request->validated();

        $Book = Book::find($borrowedBook->book_id);

        if($borrowedBook->copies > $Book->copies){
        return response()->json(
            ["message" => "Borrowed Book is Greater than Book copies"]);
        }
        else{
        $MinusCopyToBook = $Book->copies - $borrowedBook->copies;

        $borrowedBook->save();
        $Book->update(['copies' => $MinusCopyToBook]);
        return response()->json(
               ["message" => "Borrowed Book and updated Book copies",
               "data" => $borrowedBook, $Book]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrowedBook = BorrowedBook::find($id);
        return response()->json($borrowedBook);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BorrowRequest $request, $id)//This is the return book function.
    {
        $borrowedBook = BorrowedBook::find($id);
        $returnedBook = new ReturnedBook();

        $returnedBook->copies = $request->copies;
        $returnedBook->book_id = $request->book_id;
        $returnedBook->patron_id = $request->patron_id;
        $validated = $request->validated();

        $Book = Book::find($returnedBook->book_id);

        if($borrowedBook->copies == $returnedBook->copies){
            $borrowedBook->delete();
        }

        if($borrowedBook->copies < $returnedBook->copies){
            return response()->json(
                ["message" => "The number of Returned Book copies must not exceed ", $borrowedBook->copies ]);
        }

        $UpdatedCopies = $Book->copies + $returnedBook->copies;
        $borrowedBookcopies =  $borrowedBook->copies - $returnedBook->copies;
        
        $returnedBook->save();
        $borrowedBook->update(['copies' => $borrowedBookcopies]);
        $Book->update(['copies' => $UpdatedCopies]);
        return response()->json(
               ["message" => "Book Succesfully Returned"]);
    
    }
}
