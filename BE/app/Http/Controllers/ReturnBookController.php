<?php

namespace App\Http\Controllers;

use App\Models\ReturnedBook;
use Illuminate\Http\Request;

class ReturnBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $returnedBooks = ReturnedBook::all();
        return response()->json([
            "message" => "borrowedBooks List",
             "data" => $returnedBooks]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $returnedBooks = ReturnedBook::find($id);
        return response()->json($returnedBooks);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $returnedBooks = ReturnedBook::find($id);
        $returnedBooks->delete();
        return response()->json($returnedBooks);
    }
}
