import Vue from "vue";
import store from "./store";
import App from "./App.vue";
import router from "./router";
import vSelect from "vue-select";
import toastr from "@/assets/js/toaster";

Vue.config.productionTip = false;

import "bootstrap";
import "toastr/build/toastr.css";
import "bootstrap/dist/css/bootstrap.css";

Vue.use(toastr);
Vue.component("v-select", vSelect);

new Vue({
	store,
	router,
	render: (h) => h(App),
}).$mount("#app");
