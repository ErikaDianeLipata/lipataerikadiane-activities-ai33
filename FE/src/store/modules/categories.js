import axios from "@/assets/js/config/axiosConfig";

const state = {
    categories: [],
};

const getters = {
    allCategories: (state) => state.categories,
};

const mutations = {
    setCategories: (state, categories) => (state.categories = categories),
};

const actions = {
    async fetchCategories({ commit }) {
		const response = await axios.get("/categories");

		commit("setCategories", response.data);
},
};


export default{
    state,
    getters,
    mutations,
    actions
};